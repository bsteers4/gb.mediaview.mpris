## ~A gambas mpris handler and reader~ ##

2 mpris related classes.
* one to create your own mpris interface for your application.
* one for detecting/reading and controlling other mpris interfaces.

2 adapded players.
* MprisView.class is an adapted MediaView control
* In the root folde ris an archive MediaPlayer.class.tar.gz , this is an adapted MediaPlayer.class 

This package contains a fairly rubbish media player application to demonstrate the usage of MPRIS_Server.

The application can also use the MPRIS_Interface class to control other players (press the "Remote control" button)

Note: more effort was put into making the mpris classes than was put into making the demonstration applications that use them. but you sould not find it difficult to add mpris to your own wonderfully written media player app :)

<hr>

### How to use an **MPRIS_Server** in your application... ###

Best to study FMain.class for how i have used it.\
Also I have added many comments to the source to explain what's happening.

First instantize the object with an event handler name and set a unique dbus name if desired.

    MPRIS = New MPRIS_Server("gplayer") As "MPRIS"

Then set some defaults like...

    MPRIS.Identity = "My snappy title"
    MPRIS.DesktopEntry = "desktopfile-icon-name"
  
If you have not installed your own application icon then any system stock icon 
name can be used like multimedia-player

Point the (included) MprisView Media control to the class with something like...

    MprisView1.MPRIS_Server = MPRIS
    
<b>
MprisView is a copy of the MediaView component with additions to trigger
PropertyChange events to DBus when Playing/Pausing/etc and an MPRIS_Server property
that points to the attached MPRIS_Server instace
</b>

Fill in the Metadata collection
  Create a MPRIS_Metadata object then fill in its details
  Eg..
    Dim hMeta As New MPRIS_Metatadata
    hMeta.Title = "track title"
    hMeta.Artists = ["Artist1", "Artist2"]
    hMeta.Art = sPathToImage
    
    MPRIS.Metadata = hMeta

You should not use MPRIS.Metadata.objects individualy. Make an MPRIS_Metadata object then write it to MPRIS_Metadata as i have above.

Now the mpris inteface should show on the bus and have the Metadata information.

Setting the Metadata or any other property will trigger an appropriate signal to DBus.

DBus commands sent from other applications to your server will trigger the following events...\
(as demonstrated in FMain.class of this application)

    Event Fullscreen(Value As Boolean)
    Event PlayPrevious
    Event PlayNext
    Event Play
    Event PlayPause
    Event Pause
    Event Stop
    Event Pos
    Event Quit
    Event Raise
    Event Shuffle(Value As Boolean)
    Event Seek(Seconds As Float)
    Event Position(Seconds As Float)
    Event URI(Address As String)
    Event Volume(Value As Float)
    Event GetProperty(Name As String)

If you have added the event handler 'Public Sub MPRIS_Quit()' then MPRIS.CanQuit will automatically be set to true.\
The same for the other events/properties

Note:\
Event GetProperty(Name As String) **MUST** exist.\
It is called when dbus asks for deails about your player.

The simplest version of this method would look something like this..

    Public Sub MPRIS_GetProperty(Name As String)
    
      Dim v As Variant = Object.GetProperty(MprisView1, Name)
      MPRIS.Reply(v)
    
    End

That method works for MediaPlayer and the included MprisView but will fail with older 
versions of gambas that the MediaView.State property does not exist.

also included is a MediaPlayer.class that you can add to your project and 
it will add the needed things to an existing MediaPlayer object in your project.

Caveats...

Important note if you have added a TrayIcon (or any other DBus object) to 
your form in the form designer.\
If you try to initialise the mpris object in your Form_Open method or in 
its _new() method it will fail because the Forms initialization will create your
DBus interface with the wrong name.

How to get around it...

Either initialize the mpris object in your forms 'Static Public Sub _init()' method as 
this runs before the form initializes.

Or start your application via a startup.module and initiate the MPRIS_Server before opening any forms.

Or add your other DBus objects in FMain by code instead of the form designer after initializing 
the mpris object.

<hr>

### How to use the MPRIS_Interface.class to read/control other players... ###

You can use the static function **MPRIS_Interface.ListPlayers() As String[]** to get a list of all available running players.

<pre>
Dim aPlayers As String[] = MPRIS_Interface.ListPlayers()
 For Each sPlayer As String In aPlayers
    Print sPlayer
 Next
</pre>

Create a new object with an address to make a controller...

Once created you can access it's properties/methods

Eg.
<pre>

      Dim aPlayers As String[] = MPRIS_Interface.ListPlayers()
    
      If aPlayers.Count Then
        Dim hPlayer As New MPRIS_Interface(aPlayers[0])
    
        Print hPlayer.Name
        If hPlayer.Status = Media.Playing Then
          Print "Player is playing " & hPlayer.Metadata.Url
          Print "Position is " & hPlayer.Position
          Print "Duration is " & hPlayer.Metadata.Length
    
          If Message.Question("Stop the player?", "Yes", "No") = 1 Then
            hPlayer.Stop
          Endif
    
        Endif
    
      Endif

</pre>


<hr>
Todo:
more bug testing/fixing/more error handling
better documentation.
